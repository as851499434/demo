package com.example.demo.controller;

/**
 * @Author: liangfan
 * @Date: 2022/9/26 20:42
 * @Description:
 */

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rocketMq")
public class RocketMqController {

    @RequestMapping("/rocketMqProducer")
    public String rocketMqProducer(String message) {
        try {
            DefaultMQProducer producer = new DefaultMQProducer("producer_demo");
            producer.setNamesrvAddr("116.62.141.102:9876");
            producer.start();
            for (int i = 0; i < 10; i++) {
//                Thread.sleep(10000L);
                Message info =
                        new Message("TopicDemo", "TagA", ("hello rocketMq: " + i).getBytes(RemotingHelper.DEFAULT_CHARSET));
                SendResult send = producer.send(info);
//                System.out.println("我是生产者：" + JSONObject.toJSON(send));
            }
            producer.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "发送成功";
    }
}
