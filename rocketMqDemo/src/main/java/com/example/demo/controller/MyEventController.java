package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.config.event.MyEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: liangfan
 * @Date: 2022/9/28 19:33
 * @Description:
 */
@RestController
@RequestMapping("/event")
public class MyEventController {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @RequestMapping("/myEvent")
    public JSONObject myEvent(String body) {
        JSONObject jsonObject = JSONObject.parseObject(body);
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("name", "纸箱");
//        jsonObject.put("number", "200");
//        applicationEventPublisher.publishEvent(new MyEvent(jsonObject));
        return jsonObject;
    }

}
