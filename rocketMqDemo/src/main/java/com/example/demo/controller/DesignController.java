package com.example.demo.controller;

/**
 * @Author: liangfan
 * @Date: 2022/9/26 20:42
 * @Description:
 */


import com.example.demo.design.Handel.InfoHandel;
import com.example.demo.design.InfoFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class DesignController {

    @PostMapping("handel")
    public String handel(String type) {
        System.out.println("接收:" + type);
        InfoHandel handel = InfoFactory.getHandel(type);
        if (Optional.ofNullable(handel).isPresent()) {
            handel.typeHandel();
            System.out.println("结束");
        } else {
            System.out.println("没有相关处理类");
        }

        return "";
    }
}
