package com.example.demo.config.event;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @Author: liangfan
 * @Date: 2022/9/28 19:30
 * @Description:
 */
@Component
public class MyEventHandler {

    @EventListener
    public void event(MyEvent event) {
        /**
         * 括号中的参数不能为空，可以任意
         * 若为Object event，则所有事件都可以监听到
         */
        System.out.println("MyEventHandler拿到监听消息"+event.getSource());

    }
}
