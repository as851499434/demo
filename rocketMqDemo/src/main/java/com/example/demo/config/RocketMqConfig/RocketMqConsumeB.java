package com.example.demo.config.RocketMqConfig;

import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @Author: liangfan
 * @Date: 2022/9/28 19:19
 * @Description:
 */
@Component
@RocketMQMessageListener(topic = "TopicDemo",
        consumerGroup = "customerA",
        messageModel = MessageModel.CLUSTERING,
        selectorExpression = "*")
public class RocketMqConsumeB implements RocketMQListener<String> {

    /**
     * rocketmq 文档
     * https://cloud.tencent.com/document/product/1493/65383
     * @param msg
     */
    @Override
    public void onMessage(String msg) {
        System.out.println("消费者B收到消息:" + msg);
    }



}
