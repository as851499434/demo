package com.example.demo.config.event;

import org.springframework.context.ApplicationEvent;

/**
 * @Author: liangfan
 * @Date: 2022/9/28 19:21
 * @Description:
 */

public class MyEvent extends ApplicationEvent {

    private static final long serialVersionUID = 1L;

    public MyEvent(Object source) {
        super(source);
    }
}
