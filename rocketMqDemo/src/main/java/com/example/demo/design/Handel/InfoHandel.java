package com.example.demo.design.Handel;

import org.springframework.beans.factory.InitializingBean;

/**
 * 策略模式
 * @Author: liangfan
 * @Date: 2023/3/11 20:30
 * @Description:
 */

public interface InfoHandel extends InitializingBean {
    void typeHandel();
}
