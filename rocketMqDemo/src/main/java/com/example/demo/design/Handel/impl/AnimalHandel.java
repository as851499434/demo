package com.example.demo.design.Handel.impl;


import com.example.demo.design.Handel.InfoHandel;
import com.example.demo.design.InfoFactory;
import org.springframework.stereotype.Component;

/**
 * @Author: liangfan
 * @Date: 2023/3/11 20:36
 * @Description:
 */
@Component("animalHandel")
public class AnimalHandel implements InfoHandel {


    @Override
    public void typeHandel() {
        System.out.println("这是动物处理类");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        InfoFactory.register("动物", this);
    }
}
