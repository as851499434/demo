package com.example.demo.design;


import com.example.demo.design.Handel.InfoHandel;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: liangfan
 * @Date: 2023/3/11 20:10
 * @Description:
 */


public class InfoFactory {
    public static Map<String, InfoHandel>  map = new HashMap();

    public static InfoHandel getHandel(String type) {
        return map.get(type);
    }

    /**
     * 注册策略
     * @param type
     */
    public static void register(String type, InfoHandel handel) {
       if (!StringUtils.isEmpty(type)) {
           map.put(type, handel);
       }

    }
}
