/**
 * 百度公众平台开发模式(JAVA) SDK
 */
package com.example.demo.work;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

/**
 * 功能描述：https 请求 微信为https的请求
 */
@Slf4j
public class HttpKit {


    private static final String CHARSET = "UTF-8";

    /**
     * 功能描述: get 请求
     */
    public static String get(String url, Map<String, Object> params) {
        HttpRespons httpres;
        try {
            httpres = sendPost(url, "GET", params, null);
            if (httpres != null) {
                return httpres.getContent();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 功能描述：get 请求
     */
    public static String get(String url) {
        HttpRespons httpres = null;
        try {
            httpres = sendPost(url, "GET", null, null);
        } catch (IOException e) {
            System.out.println(e);
        }
        if (httpres != null) {
            return httpres.getContent();
        }
        return null;
    }

    /**
     * 功能描述: POST 请求
     */
    public static String post(String url, Map<String, Object> params) {
        HttpRespons httpres;
        try {
            httpres = sendPost(url, "POST", params, null);
            if (httpres != null) {
                return httpres.getContent();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public static String get(String url, Map<String, Object> params, Map<String, String> header) {
        HttpRespons httpres;
        try {
            httpres = sendPost(url, "GET", params, header);
            if (httpres != null) {
                return httpres.getContent();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String post(String url, Map<String, Object> params, Map<String, String> header) {
        HttpRespons httpres;
        try {
            httpres = sendPost(url, "POST", params, header);
            if (httpres != null) {
                return httpres.getContent();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * json数据格式
     */
    public static String post(String url, String params) {
        try {
            return postJson(url, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 功能描述：post请求
     */
    public static String postJson(String url, String json, Map<String, String> map) throws Exception {
        byte[] data = json.getBytes(CHARSET);
        URL urL = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) urL.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        conn.setRequestProperty("Content-Length", String.valueOf(data.length));
        for (Entry<String, String> entry : map.entrySet()) {
            conn.setRequestProperty(entry.getKey(), entry.getValue());
        }
        conn.setConnectTimeout(5 * 1000);
        OutputStream outStream = conn.getOutputStream();
        outStream.write(data);
        outStream.flush();
        outStream.close();
        if (conn.getResponseCode() == 200) {
            byte[] read2Byte = read2Byte(conn.getInputStream());
            return new String(read2Byte, CHARSET);
        } else {
            HttpStatus hs = HttpStatus.valueOf(conn.getResponseCode());
            throw new HttpClientErrorException(hs, "返回数据出错：" + hs);
        }
        //return null;
    }

    public static String postJson(String url, String json) throws Exception {
        return postJson(url, json, new HashMap());
    }

    /**
     * 二进制数据转换
     *
     * @param inStream
     * @return
     * @throws Exception
     */
    public static byte[] read2Byte(InputStream inStream) throws Exception {
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();
        return outSteam.toByteArray();
    }

    /**
     * 发送HTTP请求
     *
     * @param urlString  地址
     * @param method     get/post
     * @param parameters 添加由键值对指定的请求参数
     * @param propertys  添加由键值对指定的一般请求属性
     * @return 响映对象
     * @throws IOException
     */
    private static HttpRespons sendPost(String urlString, String method, Map<String, Object> parameters,
                                        Map<String, String> propertys) throws IOException {
        HttpURLConnection urlConnection = null;

        if (method.equalsIgnoreCase("GET") && parameters != null) {
            StringBuffer param = new StringBuffer();
            int i = 0;
            for (Object key : parameters.keySet()) {
                if (key.equals("Cookie")) {
                    continue;
                }
                if (i == 0)
                    param.append("?");
                else
                    param.append("&");
                param.append(key).append("=").append(parameters.get(key));
                i++;
            }
            urlString += param;
        }

        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection) url.openConnection();
        if (parameters != null && parameters.get("Cookie") != null) {
            urlConnection.setRequestProperty("Cookie", (String) parameters.get("Cookie"));
        }
        urlConnection.setRequestMethod(method);
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        urlConnection.setUseCaches(false);

        if (propertys != null)
            for (String key : propertys.keySet()) {
                urlConnection.addRequestProperty(key, propertys.get(key));
            }

        if (method.equalsIgnoreCase("POST") && parameters != null) {
            StringBuffer param = new StringBuffer();
            for (String key : parameters.keySet()) {
                param.append("&");
                param.append(key).append("=").append(parameters.get(key));
            }
            urlConnection.getOutputStream().write(param.toString().getBytes());
            urlConnection.getOutputStream().flush();
            urlConnection.getOutputStream().close();
        }
        return makeContent(urlString, urlConnection);
    }

    /**
     * 得到响应对象
     *
     * @param urlConnection
     * @return 响应对象
     * @throws IOException
     */
    private static HttpRespons makeContent(String urlString, HttpURLConnection urlConnection) throws IOException {
        HttpRespons httpResponser = new HttpRespons();
        try {
            InputStream in = urlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            httpResponser.contentCollection = new Vector<String>();
            StringBuffer temp = new StringBuffer();
            String line = bufferedReader.readLine();
            while (line != null) {
                httpResponser.contentCollection.add(line);
                temp.append(line).append("\r\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            String ecod = urlConnection.getContentEncoding();
            if (ecod == null)
                ecod = CHARSET;
            httpResponser.urlString = urlString;
            httpResponser.defaultPort = urlConnection.getURL().getDefaultPort();
            httpResponser.file = urlConnection.getURL().getFile();
            httpResponser.host = urlConnection.getURL().getHost();
            httpResponser.path = urlConnection.getURL().getPath();
            httpResponser.port = urlConnection.getURL().getPort();
            httpResponser.protocol = urlConnection.getURL().getProtocol();
            httpResponser.query = urlConnection.getURL().getQuery();
            httpResponser.ref = urlConnection.getURL().getRef();
            httpResponser.userInfo = urlConnection.getURL().getUserInfo();
            httpResponser.content = new String(temp.toString().getBytes(), ecod);
            httpResponser.contentEncoding = ecod;
            httpResponser.code = urlConnection.getResponseCode();
            httpResponser.message = urlConnection.getResponseMessage();
            httpResponser.contentType = urlConnection.getContentType();
            httpResponser.method = urlConnection.getRequestMethod();
            httpResponser.connectTimeout = urlConnection.getConnectTimeout();
            httpResponser.readTimeout = urlConnection.getReadTimeout();
            return httpResponser;
        } catch (IOException e) {
            throw e;
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
    }

    /**
     * post数据【topic-type=application/x-www-form-urlencoded】
     *
     * @param params
     * @param URL
     * @return
     */
    public static String sendHttptoServer(String URL, Map<String, String> params) {
        StringBuffer sub = new StringBuffer();
        String params_str = null;
        if (params != null) {
            for (Entry<String, String> entry : params.entrySet()) {
                sub.append(entry.getKey());
                sub.append("=");
                sub.append(entry.getValue());
                sub.append("&");
            }
            params_str = sub.substring(0, sub.length() - 1);
        }
        log.info("send url=" + URL);
        log.info("send params_str=" + params_str);

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = null;
        BufferedWriter out = null;
        try {
            java.net.URL url = new URL(URL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setConnectTimeout(30000);   //设置延迟为30秒
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.connect();
            OutputStreamWriter reqOut = null;
            if (params_str != null) {
                reqOut = new OutputStreamWriter(httpURLConnection.getOutputStream(), "UTF-8");
            }
            out = new BufferedWriter(reqOut);
            out.write(params_str.toString());
            out.flush();
            out.close();
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            // 接收服务器的返回：
            reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "utf-8"));

            String line = null;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
        } catch (Exception e) {
            log.error("请求外部url=" + URL + "发生未知异常", e);
        } finally {//关闭流
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return buffer.toString();
    }

    public static void main(String[] args) throws Exception {
        // String accessToken =
        // "ulhEL9F2CciJezmGj47C-d3hAJZwXiAANctVIwSHwBRK7Z1enIRWeZKZekk8jS5abIkCo2YmMSDlqUFKOKvSaw";
        // String openId = "oeZTVt6XlCphRnCI-DlpdTyk27p4";
        // UserInfo u = WeChat.user.getUserInfo(accessToken, openId);
        // logger.info(JSON.toJSONString(u));
        // logger.info(WeChat.message.sendText(accessToken , openId ,
        // "测试"));
        // Map<String, Object> mgs = WeChat.uploadMedia(accessToken, "image",
        // new File("C:\\Users\\郭华\\Pictures\\13.jpg"));
        // logger.info(JSON.toJSONString(mgs));
    }
}
