package com.example.demo.work;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author: liangfan
 * @Date: 2022/9/30 11:19
 * @Description:
 */

@Getter
@Setter
@EqualsAndHashCode
public class DemoData {
    private String merchantUserId;
    private String merchantUserName;
    private String status;
}